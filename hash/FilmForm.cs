﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace hash
{
    public partial class FilmForm : Form
    {
        private FormState FormState;

        public FilmForm(FormState formState, Film f = null)
        {
            InitializeComponent();
            FormState = formState;
            switch (formState)
            {
                case FormState.ADD:
                    {
                        button1.Visible = true;
                        textBox1.Text = "";
                        textBox2.Text = "";
                        textBox4.Text = "";
                        button3.Visible = false;
                        button4.Visible = false;
                        list = new List<string>();
                        break;
                    }
                case FormState.EDIT:
                    {
                        textBox1.ReadOnly = true;
                        textBox1.Text = f.title;
                        textBox2.Text = f.director;
                        button1.Visible = true;
                        button3.Visible = true;
                        button4.Visible = true;
                        textBox4.Text = f.summary;
                        list = f.actors;
                        break;
                    }
                case FormState.DISPLAY:
                    {
                        textBox1.ReadOnly = true;
                        textBox1.Text = f.title;
                        textBox2.ReadOnly = true;
                        textBox2.Text = f.director;
                        textBox4.ReadOnly = true;
                        textBox4.Text = f.summary;
                        button1.Visible = false;
                        button3.Visible = false;
                        list = f.actors;
                        break;
                    }
            }
        }

        public Film f;
        public bool isRightClosed = false;
        public List<string> list;

        //закрытие формы
        private void button2_Click(object sender, EventArgs e)
        {
            f = new Film();
            if (textBox1.Text == "" || textBox2.Text == "" || list.Count == 0 || textBox4.Text == "")
            {
                MessageBox.Show("Введены неверные данные", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                isRightClosed = false;
            }
            else
            {
                f.title = textBox1.Text;
                f.director = textBox2.Text;
                f.actors = list;
                f.summary = textBox4.Text;
                isRightClosed = true;
            }
            Close();
        }

        //добавление актера
        private void button1_Click(object sender, EventArgs e)
        {
            StringForm strForm = new StringForm(FormState1.ACTOR);
            strForm.ShowDialog();
            if (strForm.str != "")
            {
                list.Add(strForm.str);
            }
            else
            {
                MessageBox.Show("Введены неверные данные", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //удаление актера
        private void button3_Click(object sender, EventArgs e)
        {
            StringForm strForm = new StringForm(FormState1.ACTOR);
            strForm.ShowDialog();
            if (strForm.str != "")
            {
                list.Remove(strForm.str);
            }
            else
            {
                MessageBox.Show("Введены неверные данные", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //вывод списка актеров
        private void button4_Click(object sender, EventArgs e)
        {
            if (list.Count == 0)
            {
                MessageBox.Show("Пуст", "Список актеров", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            else
            {
                string allActors = "";
                foreach (string s in list)
                {
                    allActors += s + "\n";
                }
                MessageBox.Show(allActors, "Список актеров", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
