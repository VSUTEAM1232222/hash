﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hash
{
    public class Film
    {
        public string title { get; set; }
        public string director { get; set; }
        public List<string> actors { get; set; }
        public string summary { get; set; }
        
        //конструктор
        public Film()
        {
            title = "";
            director = "";
            actors = new List<string>();
            summary = "";
        }
    }
}
